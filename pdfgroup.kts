/**
 * Joins PDFs
 *     Senecio_Gemeinden_mitZustaendigkeit_Bülach_KarteNr_01.pdf
 *     Senecio_Gemeinden_mitZustaendigkeit_Bülach_KarteNr_02.pdf
 *   etc.
 *   to
 *     out/Senecio_Gemeinden_mitZustaendigkeit_Bülach_KarteNr_.pdf
 * sudo pacman -S kotlin
 * Usage: kotlinc -script pdfgroup.kts <InputDirectory>
 **/


import java.io.File
import java.io.InputStream

val directory = File(args[0])
val files = File(args[0]).listFiles { file ->
    !file.isDirectory()
            && file.name.toLowerCase().endsWith(".pdf")
}

val grouped = files.groupBy { f -> removeLastChars(3, f.nameWithoutExtension) }.toMap()

val outFolder = File("${directory.absolutePath}/out/")
if (!outFolder.exists()) {
    outFolder.mkdirs()
}

grouped.forEach { groupname, list ->
    println("###### Concat: $groupname")
    val inFilesParam = list.map { f -> "-f ${f.absoluteFile}" }.sorted().joinToString(" ")
    //println("### iFilesParam: $inFilesParam")

    val outFileParam = "-o ${outFolder.absolutePath}/${groupname.replace("_KarteNr","")}.pdf"
    //println("### outFileParam: $outFileParam")

    val cmd = "pdfsam-legacy-console $inFilesParam $outFileParam concat"
    println("### cmd: $cmd")

    val process = Runtime.getRuntime().exec(cmd)
    printStdOut(process.errorStream)
    process.waitFor()
}

fun printStdOut(stream: InputStream) {
    val inputAsString = stream.bufferedReader().use { it.readText() }
    println(inputAsString)
}

fun removeLastChars(nr: Int, str: String): String {
    return str.substring(0, str.length - nr)
}