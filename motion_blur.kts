/**
 * Smooths Video using vid.stab
 * sudo pacman -S vid.stab
 * Usage: kotlinc -script file.kts <File>
 **/


import java.io.File
import java.io.InputStreamReader
import java.io.BufferedReader
import com.sun.xml.internal.ws.streaming.XMLStreamReaderUtil.close
import jdk.nashorn.internal.runtime.ScriptingFunctions.readLine
import java.io.InputStream


val files = File(args[0]).listFiles { file ->
    !file.isDirectory()
            && file.name.toLowerCase().endsWith(".mp4")
}

files?.forEach { file ->
    val outFile = "${file.parentFile.absolutePath}/out/${file.nameWithoutExtension}_motionblur.mp4"
    val inFile = "${file.absoluteFile}"
    println("Processing: $file")
    println("Out:        $outFile")

    val outFolder = File("${file.parentFile.absolutePath}/out/")
    if(!outFolder.exists()){
        outFolder.mkdirs()
    }

    val analyzingCmd = "ffmpeg -y -i $inFile -vf split[v1][v2],[v1]select='eq(mod(n,2),0)'[v1],[v2]select='eq(mod(n,2),1)'[v2],[v1][v2]mix,split[v1][v2],[v1]select='eq(mod(n,2),0)'[v1],[v2]select='eq(mod(n,2),1)'[v2],[v1][v2]mix,split[v1][v2],[v1]select='eq(mod(n,2),0)'[v1],[v2]select='eq(mod(n,2),1)'[v2],[v1][v2]mix,split[v1][v2],[v1]select='eq(mod(n,2),0)'[v1],[v2]select='eq(mod(n,2),1)'[v2],[v1][v2]mix $outFile"
    println("###### Motion Bluring: $file")
    println("###### CMD: $analyzingCmd")
    val motionBluProcess = Runtime.getRuntime().exec(analyzingCmd)
    printStdOut(motionBluProcess.errorStream)
    motionBluProcess.waitFor()
}

fun printStdOut(stream: InputStream) {
    val inputAsString = stream.bufferedReader().use { it.readText() }
    println(inputAsString)
}
