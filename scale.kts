/**
 * Scale pngs to AxApx (eg A=32 -> 32x32)
 * sudo pacman -S kotlin
 * Usage: kotlinc -script scale.kts <InputDirectory> <scale>
 **/


import java.io.File
import java.io.InputStream

println("### args: $args")

val directory = File(args[0])
val newSize = args[1]
val files = File(args[0]).listFiles { file ->
    !file.isDirectory() && file.name.toLowerCase().endsWith(".png")
}

val outFolder = File("${directory.absolutePath}/out/")
if (!outFolder.exists()) {
    outFolder.mkdirs()
}


for (file in files) {
    val cmd = "convert ${file.absolutePath} -resize ${newSize}x${newSize} $outFolder/${file.name}"
    println("### cmd: $cmd")

    val process = Runtime.getRuntime().exec(cmd)

    printStdOut(process.errorStream)
    process.waitFor()
}

fun printStdOut(stream: InputStream) {
    val inputAsString = stream.bufferedReader().use { it.readText() }
    println(inputAsString)
}
