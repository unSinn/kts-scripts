/**
 * Smooths Video using vid.stab
 * sudo pacman -S vid.stab
 * Usage: kotlinc -script file.kts <Folder>
 **/


import java.io.File
import java.io.InputStreamReader
import java.io.BufferedReader
import com.sun.xml.internal.ws.streaming.XMLStreamReaderUtil.close
import jdk.nashorn.internal.runtime.ScriptingFunctions.readLine
import java.io.InputStream


val motionblurEnabled = false
val forceTrf = true

val motionblur = when {
    motionblurEnabled -> ",minterpolate=50,tblend=all_mode=average,framestep=2"
    else -> ""
}

val files = File(args[0]).listFiles { file ->
    !file.isDirectory()
            && file.name.toLowerCase().endsWith(".mp4")
}
files?.forEach { file ->
    val outFile = "${file.parentFile.absolutePath}/out/${file.nameWithoutExtension}_smooth.mp4"
    val inFile = "${file.absoluteFile}"
    val trf = "${file.absolutePath}.trf"
    println("Processing: $file")
    println("TRF:        $trf")
    println("Out:        $outFile")

    val outFolder = File("${file.parentFile.absolutePath}/out/")
    if (!outFolder.exists()) {
        outFolder.mkdirs()
    }


    if (!File(trf).exists() || !forceTrf) {
        val analyzingCmd = "ffmpeg -y -i $inFile -vf vidstabdetect=result=$trf -f null -"
        println("###### Analyzing: $file")
        println("###### CMD: $analyzingCmd")
        val trfProcess = Runtime.getRuntime().exec(analyzingCmd)
        printStdOut(trfProcess.errorStream)
        trfProcess.waitFor()
    }

    val filters = "vidstabtransform=smoothing=50:input=$trf,unsharp=5:5:0.8:3:3:0.4$motionblur"

    val renderCmd = "ffmpeg -y -i $inFile -vf $filters $outFile"
    println("###### Rendering: $file")
    println("###### CMD: $renderCmd")
    val renderProcess = Runtime.getRuntime().exec(renderCmd)
    printStdOut(renderProcess.errorStream)
}

fun printStdOut(stream: InputStream) {
    val inputAsString = stream.bufferedReader().use { it.readText() }
    println(inputAsString)
}
